package puzzle.ads;

import java.io.IOException;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * A class to extend Media Player and implement handling for interfaces needed.
 * I also started implementing the ability to handle the sound changes due to
 * incoming notification sounds like phone or message alerts *
 * 
 * Should handle errors with headphones becoming unplugged and media player
 * states with opening and closing of application.
 * 
 * @author Rick
 * 
 */
public class MyMediaPlayer extends MediaPlayer implements
		MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
		AudioManager.OnAudioFocusChangeListener,
		MediaPlayer.OnCompletionListener {

	CommonVariables cv = CommonVariables.getInstance();
	public MediaPlayer mediaPlayer;
	Uri path = Uri.parse(Data.PATH + Data.TRACK_01);
	public Toast toast;
	AudioManager audioManager;

	public void init() {
		Log.d("MyMediaPlayer", "init start");

		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnPreparedListener(this);
		mediaPlayer.setOnErrorListener(this);
		Log.d("MyMediaPlayer", "init start");
	}

	public void start() {
		Log.d("MyMediaPlayer", "start start");

		try {
			// if we don't have to play music then don't take focus
			if (cv.playMusic) {
				audioManager = (AudioManager) cv.context
						.getSystemService(Context.AUDIO_SERVICE);
				int result = audioManager
						.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
								AudioManager.AUDIOFOCUS_GAIN);
				if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
					// Start play back.
					mediaPlayer.reset();
					mediaPlayer.setDataSource(cv.context, path);
					mediaPlayer.setLooping(true);
					mediaPlayer.setVolume(cv.volume, cv.volume);
					mediaPlayer.prepareAsync();
				}
			}

		} catch (IllegalArgumentException | SecurityException
				| IllegalStateException | IOException e) {
			// try is for the setting of the data source
			Log.d("MyMediaPlayer", "catch Exception line 74");
			init();
			start();
		}
		Log.d("MyMediaPlayer", "init end");
	}

	@Override
	public void onPrepared(MediaPlayer player) {
		Log.d("MyMediaPlayer", "onPrepared start");

		// check for option to play music and resume last position
		if (cv.playMusic) {
			if (cv.currentSoundPosition > 0)
				mediaPlayer.seekTo(cv.currentSoundPosition);
			player.start();
		}
		Log.d("MyMediaPlayer", "onPrepared end");
	}

	public void setSoundStopped() {
		Log.d("MyMediaPlayer", "onPrepared start");
		// set current position to be the media player position
		if (mediaPlayer != null) {
			cv.currentSoundPosition = mediaPlayer.getCurrentPosition();

			// check if position is over the valid duration
			if (cv.currentSoundPosition >= mediaPlayer.getDuration()
					&& mediaPlayer.getDuration() != -1)
				cv.currentSoundPosition = 0;
		}
		Log.d("MyMediaPlayer", "onPrepared end");
	}

	/**
	 * 
	 */
	@Override
	public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
		// handle errors that might be specific to implementation
		Log.d("MyMediaPlayer", "onError " + i + "," + i2 + " start");

		switch (i) {
		case MediaPlayer.MEDIA_ERROR_UNKNOWN:
			break;
		case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
			break;
		default:
			break;
		}

		switch (i2) {
		case MediaPlayer.MEDIA_ERROR_IO:
			break;
		case MediaPlayer.MEDIA_ERROR_MALFORMED:
			break;
		case MediaPlayer.MEDIA_ERROR_UNSUPPORTED:
			break;
		case MediaPlayer.MEDIA_ERROR_TIMED_OUT:
			break;
		default:
			break;
		}

		mediaPlayer.reset();
		start();

		Log.d("MyMediaPlayer", "onError " + i + "," + i2 + " end");
		return true;
	}

	@Override
	public void onAudioFocusChange(int focusChange) {
		Log.d("MyMediaPlayer", "onAudioFocusChange start");
		// Handle audio lowering and raising for other phone sounds
		switch (focusChange) {
		case AudioManager.AUDIOFOCUS_GAIN:
			// resume play back
			if (mediaPlayer == null)
				init();
			else if (!mediaPlayer.isPlaying()) {
				setNewVolume(1.0f);
				start();
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS:
			// lost focus for an unbounded amount of time. stop and release
			if (mediaPlayer != null) {
				if (mediaPlayer.isPlaying()) {
					mediaPlayer.stop();
					setSoundStopped();
				}
				mediaPlayer.release();
				mediaPlayer = null;
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
			// lost focus for a short time, but we have to stop play back.
			if (mediaPlayer != null && mediaPlayer.isPlaying()) {
				mediaPlayer.pause();
				setSoundStopped();
			}
			break;
		case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
			if (mediaPlayer != null && mediaPlayer.isPlaying()) {
				setNewVolume(0.1f);
			}
			break;
		}
		Log.d("MyMediaPlayer", "onAudioFocusChange end");
	}

	public void resume() {
		Log.d("MyMediaPlayer", "resume start");
		init();
		start();
		Log.d("MyMediaPlayer", "resume end");

	}

	public void destroy() {
		Log.d("MyMediaPlayer", "destroy start");
		// a final check when the app closes down for good
		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
		Log.d("MyMediaPlayer", "destroy end");
	}

	public void setNewVolume(Float setVolume) {
		// sets the new volume and updates the audio manager
		mediaPlayer.setVolume(setVolume, setVolume);
	}

	public void toggleMusic() {
		if (cv.playMusic) {
			cv.playMusic = false;
			cv.showToast("Music off");
			pause();
		} else {
			cv.playMusic = true;
			cv.showToast("Music on/restarted");
			resume();
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// the media player may error out and reach here if not handled
		Log.d("MyMediaPlayer", "onCompletion end");
		init();
		start();
	}

	public void quietSound() {
		if (mediaPlayer.isPlaying()) {
			setNewVolume(0.1f);
		}
	}

	public void cleanUp() {
		// a final check when the app closes down for good
		if (mediaPlayer != null) {
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}

	public void onPause() {
		Log.d("MyMediaPlayer", "pause start");
		if (audioManager != null)
			audioManager.abandonAudioFocus(this);
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying())
				mediaPlayer.pause();
			setSoundStopped();
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		}
		Log.d("MyMediaPlayer", "pause end");
	}
}