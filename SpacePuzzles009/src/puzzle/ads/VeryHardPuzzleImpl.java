package puzzle.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.graphics.Bitmap;
import android.view.MotionEvent;

/**
 * 
 * A class with very hard implementation is a 5 x 5 grid.
 * 
 * @author Rick
 * 
 */
public class VeryHardPuzzleImpl implements Puzzle {

	public int PIECES = 25;
	int bitmapWd5;
	int bitmapHd5;

	public int piecesComplete;
	int jumbledNumber;
	int index;
	boolean isRandom, newImageComplete;
	public Date startPuzzle = new Date();
	public Date stopPuzzle = new Date();
	long currPuzzleTime = 0;
	CommonVariables common = CommonVariables.getInstance();

	public VeryHardPuzzleImpl() {
	}

	public void getNewImageLoadedScaledDivided(Thread thread) {
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {

				while (!newImageComplete) {
					// fill with all valid numbers
					if (common.imagesShown.isEmpty())
						for (int i = 0; i < Data.PICS.length; i++)
							common.imagesShown.add(i);

					// get new index value from remaining images
					index = common.rand.nextInt(common.imagesShown.size());
					// get the value at that index for new image
					common.currentPuzzleImagePosition = common.imagesShown
							.get(index);
					// remove from list to prevent duplicates
					common.imagesShown.remove(index);

					common.image = common.decodeSampledBitmapFromResource(
							common.res,
							Data.PICS[common.currentPuzzleImagePosition],
							common.screenW, common.screenH);
					common.image = Bitmap.createScaledBitmap(common.image,
							common.screenW, common.screenH, true);

					newImageComplete = divideBitmap();

					if (newImageComplete) {
						resetTimer();
						common.errorLoading = false;
						common.solved = false;
						common.imageReady = true;
						System.gc();
					} else {
						common.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	public boolean divideBitmap() {
		common.numberOfPieces = PIECES;

		common.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzlePieces[i] = new PuzzlePiece();

		common.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzleSlots[i] = new PuzzleSlot();

		common.slotOrder = new int[PIECES];

		// set natural order
		for (int i = 0; i < PIECES; i++)
			common.slotOrder[i] = i;

		isRandom = false;
		while (!isRandom) {
			List<Integer> list = new ArrayList<Integer>();
			for (int i : common.slotOrder) {
				list.add(i);
			}

			Collections.shuffle(list);
			for (int i = 0; i < list.size(); i++) {
				common.slotOrder[i] = list.get(i);
			}

			for (int i = 0; i < common.slotOrder.length; i++) {
				if (common.slotOrder[i] != i)
					isRandom = true;
			}
			//
			// if (cv.slotOrder[0] == 0 && cv.slotOrder[1] == 1
			// && cv.slotOrder[2] == 2 && cv.slotOrder[3] == 3
			// && cv.slotOrder[4] == 4 && cv.slotOrder[5] == 5
			// && cv.slotOrder[6] == 6 && cv.slotOrder[7] == 7
			// && cv.slotOrder[8] == 8 && cv.slotOrder[9] == 9
			// && cv.slotOrder[10] == 10 && cv.slotOrder[11] == 11
			// && cv.slotOrder[12] == 12 && cv.slotOrder[13] == 13
			// && cv.slotOrder[14] == 14 && cv.slotOrder[15] == 15
			// && cv.slotOrder[16] == 2 && cv.slotOrder[16] == 3
			// && cv.slotOrder[17] == 4 && cv.slotOrder[17] == 5
			// && cv.slotOrder[18] == 6 && cv.slotOrder[18] == 18
			// && cv.slotOrder[20] == 20 && cv.slotOrder[19] == 19
			// && cv.slotOrder[21] == 21 && cv.slotOrder[22] == 22
			// && cv.slotOrder[24] == 24 && cv.slotOrder[23] == 23) {
			// isRandom = false;
			// }
		}

		// re do if the image didn't split correctly
		common.imageSplit = false;
		piecesComplete = 0;

		while (!common.imageSplit) {
			int w = common.image.getWidth();
			int h = common.image.getHeight();
			bitmapWd5 = w / 5;
			bitmapHd5 = h / 5;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 5) {
					y = 0;
				} else if (i < 10) {
					y = bitmapHd5;
				} else if (i < 15) {
					y = bitmapHd5 * 2;
				} else if (i < 20) {
					y = bitmapHd5 * 3;
				} else {
					y = bitmapHd5 * 4;
				}

				x = (i % 5) * bitmapWd5;

				if (common.puzzlePieces[i].bitmap != null)
					common.puzzlePieces[i].bitmap.recycle();

				common.puzzlePieces[i].bitmap = null;
				common.puzzlePieces[i].bitmap = Bitmap.createBitmap(
						common.image, x, y, bitmapWd5, bitmapHd5);

				common.puzzlePieces[i].px = x;
				common.puzzlePieces[i].px2 = x + bitmapWd5;

				common.puzzlePieces[i].py = y;
				common.puzzlePieces[i].py2 = y + bitmapHd5;

				common.puzzleSlots[i].sx = x;
				common.puzzleSlots[i].sx2 = x + bitmapWd5;

				common.puzzleSlots[i].sy = y;
				common.puzzleSlots[i].sy2 = y + bitmapHd5;

				common.puzzleSlots[i].puzzlePiece = common.puzzlePieces[i];
				common.puzzleSlots[i].slotNum = common.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			common.imageSplit = true;
			common.image.recycle();
			common.image = null;
		}

		common.jumblePicture();

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean divideBitmapFromPreviousPuzzle() {
		common.numberOfPieces = PIECES;

		common.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzlePieces[i] = new PuzzlePiece();

		common.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzleSlots[i] = new PuzzleSlot();

		// re do if the image didn't split correctly
		common.imageSplit = false;
		piecesComplete = 0;

		while (!common.imageSplit) {
			int w = common.image.getWidth();
			int h = common.image.getHeight();
			bitmapWd5 = w / 5;
			bitmapHd5 = h / 5;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 5) {
					y = 0;
				} else if (i < 10) {
					y = bitmapHd5;
				} else if (i < 15) {
					y = bitmapHd5 * 2;
				} else if (i < 20) {
					y = bitmapHd5 * 3;
				} else {
					y = bitmapHd5 * 4;
				}

				x = (i % 5) * bitmapWd5;

				if (common.puzzlePieces[i].bitmap != null)
					common.puzzlePieces[i].bitmap.recycle();

				common.puzzlePieces[i].bitmap = null;
				common.puzzlePieces[i].bitmap = Bitmap.createBitmap(
						common.image, x, y, bitmapWd5, bitmapHd5);

				common.puzzlePieces[i].px = x;
				common.puzzlePieces[i].px2 = x + bitmapWd5;

				common.puzzlePieces[i].py = y;
				common.puzzlePieces[i].py2 = y + bitmapHd5;

				common.puzzleSlots[i].sx = x;
				common.puzzleSlots[i].sx2 = x + bitmapWd5;

				common.puzzleSlots[i].sy = y;
				common.puzzleSlots[i].sy2 = y + bitmapHd5;

				// cv.puzzleSlots[i].puzzlePiece = cv.puzzlePieces[i];
				common.puzzleSlots[i].slotNum = common.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			common.imageSplit = true;
			common.image.recycle();
			common.image = null;
		}

		// set puzzle piece to a new slot by creating separate puzzle pieces and
		// reassign individually
		boolean correctlyReassembled = false;
		while (!correctlyReassembled) {
			// use saved slot list to sort
			for (int toSlot = 0; toSlot < common.slotOrder.length; toSlot++) {

				// get new slot to take piece from and place into correct slot
				int fromSlot = common.slotOrder[toSlot];
				PuzzlePiece pieceA = common.puzzlePieces[fromSlot];

				common.puzzleSlots[toSlot].puzzlePiece = pieceA;
				common.puzzleSlots[toSlot].puzzlePiece.px = common.puzzleSlots[toSlot].sx;
				common.puzzleSlots[toSlot].puzzlePiece.py = common.puzzleSlots[toSlot].sy;
				common.puzzleSlots[toSlot].puzzlePiece.px2 = common.puzzleSlots[toSlot].sx2;
				common.puzzleSlots[toSlot].puzzlePiece.py2 = common.puzzleSlots[toSlot].sy2;
				common.puzzleSlots[toSlot].puzzlePiece.pieceNum = fromSlot;
			}

			correctlyReassembled = true;
			for (int j = 0; j < common.puzzleSlots.length; j++) {
				int a = common.puzzleSlots[j].puzzlePiece.pieceNum;
				int b = common.slotOrder[j];
				if (a != b) {
					correctlyReassembled = false;
				}
			}
		}
		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int newx = (int) event.getX();
		int newy = (int) event.getY();

		// find the piece that was pressed down onto
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			common.movingPiece = false;
			if (newx < bitmapWd5) {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 0;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 5;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 10;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 15;
				} else {
					common.currPieceOnTouch = 20;
				}
			} else if (newx < bitmapWd5 * 2) {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 1;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 6;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 11;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 16;
				} else {
					common.currPieceOnTouch = 21;
				}
			} else if (newx < bitmapWd5 * 3) {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 2;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 7;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 12;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 17;
				} else {
					common.currPieceOnTouch = 22;
				}
			} else if (newx < bitmapWd5 * 4) {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 3;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 8;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 13;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 18;
				} else {
					common.currPieceOnTouch = 23;
				}
			} else {
				if (newy < bitmapHd5) {
					common.currPieceOnTouch = 4;
				} else if (newy < bitmapHd5 * 2) {
					common.currPieceOnTouch = 9;
				} else if (newy < bitmapHd5 * 3) {
					common.currPieceOnTouch = 14;
				} else if (newy < bitmapHd5 * 4) {
					common.currPieceOnTouch = 19;
				} else {
					common.currPieceOnTouch = 24;
				}
			}
		}

		if (event.getAction() == MotionEvent.ACTION_UP) {
			common.movingPiece = false;
			if (newx < bitmapWd5) {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 0;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 5;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 10;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 15;
				} else {
					common.currSlotOnTouchUp = 20;
				}
			} else if (newx < bitmapWd5 * 2) {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 1;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 6;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 11;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 16;
				} else {
					common.currSlotOnTouchUp = 21;
				}
			} else if (newx < bitmapWd5 * 3) {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 2;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 7;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 12;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 17;
				} else {
					common.currSlotOnTouchUp = 22;
				}
			} else if (newx < bitmapWd5 * 4) {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 3;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 8;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 13;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 18;
				} else {
					common.currSlotOnTouchUp = 23;
				}
			} else {
				if (newy < bitmapHd5) {
					common.currSlotOnTouchUp = 4;
				} else if (newy < bitmapHd5 * 2) {
					common.currSlotOnTouchUp = 9;
				} else if (newy < bitmapHd5 * 3) {
					common.currSlotOnTouchUp = 14;
				} else if (newy < bitmapHd5 * 4) {
					common.currSlotOnTouchUp = 19;
				} else {
					common.currSlotOnTouchUp = 24;
				}
			}

			// check for image to be in new slot
			if (common.currPieceOnTouch != common.currSlotOnTouchUp) {
				common.sendPieceToNewSlot(common.currPieceOnTouch,
						common.currSlotOnTouchUp);
				common.mySoundPool.playTapSound();
			} else {
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px = common.puzzleSlots[common.currSlotOnTouchUp].sx;
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py = common.puzzleSlots[common.currSlotOnTouchUp].sy;
			}
		}

		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			common.movingPiece = true;
			if (common.currPieceOnTouch >= 0
					|| common.currPieceOnTouch <= PIECES - 1) {
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px = newx
						- bitmapWd5 / 2;
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py = newy
						- bitmapHd5 / 2;
			}
		}

		common.inPlace = 0;
		for (int i = 0; i < common.numberOfPieces; i++) {
			if (common.puzzleSlots[i].slotNum == common.puzzleSlots[i].puzzlePiece.pieceNum) {
				common.inPlace++;
			}
		}

		if (common.inPlace == common.numberOfPieces) {
			stopTimer();
			common.solved = true;
			common.showToast(""
					+ Data.PIC_NAMES[common.currentPuzzleImagePosition]
					+ " solve Time: " + getSolveTime());
			return false;
		}

		return true;
	}

	@Override
	public void recylceAll() {
		if (common.image != null)
			common.image.recycle();

		for (int i = 0; i < common.puzzlePieces.length; i++)
			if (common.puzzlePieces != null)
				if (common.puzzlePieces[i] != null)
					if (common.puzzlePieces[i].bitmap != null)
						common.puzzlePieces[i].bitmap.recycle();

	}

	@Override
	public String getPercentComplete() {
		return "" + (float) piecesComplete / PIECES;
	}

	@Override
	public void initTimer() {
		startPuzzle = new Date();
	}

	@Override
	public void stopTimer() {
		stopPuzzle = new Date();
		currPuzzleTime += stopPuzzle.getTime() - startPuzzle.getTime();
	}

	@Override
	public void resetTimer() {
		currPuzzleTime = 0;
		startPuzzle = new Date();
	}

	@Override
	public double getSolveTime() {
		return currPuzzleTime / 1000.0;
	}

	@Override
	public int getCurrentImage() {
		// return current image index
		return index;
	}

	@Override
	public void getPrevousImageLoadedScaledDivided(Thread thread) {
		// If the database returns with valid values the game can be resumed
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {
				while (!newImageComplete) {
					// get new index value and then remove index
					index = common.currentPuzzleImagePosition;

					common.image = common.decodeSampledBitmapFromResource(
							common.res,
							Data.PICS[common.currentPuzzleImagePosition],
							common.screenW, common.screenH);

					common.image = Bitmap.createScaledBitmap(common.image,
							common.screenW, common.screenH, true);

					newImageComplete = divideBitmapFromPreviousPuzzle();

					if (newImageComplete) {
						resetTimer();
						common.errorLoading = false;
						common.solved = false;
						common.imageReady = true;
						System.gc();
					} else {
						common.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	@Override
	public void pause() {
		stopTimer();
	}
}