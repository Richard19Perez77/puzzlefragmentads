package puzzle.ads;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;

/**
 * 
 * The save photo class allows for the user to save a high resolution version of
 * the puzzle they just solved.
 * 
 * @author Rick
 *
 */
public class SavePhoto extends Thread {

	CommonVariables common = CommonVariables.getInstance();
	int currentImageToSave;

	public SavePhoto(int position) {
		currentImageToSave = position;
	}

	private boolean mExternalStorageAvailable = false;
	private boolean mExternalStorageWriteable = false;

	@Override
	public void run() {
		super.run();

		// save current image to devices images folder
		String state = Environment.getExternalStorageState();
		// check if writing is an option
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// We can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			// We can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			// Something else is wrong. It may be one of many other states, but
			// all we need to know is we can neither read nor write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}

		if (mExternalStorageAvailable && mExternalStorageWriteable) {
			// then write picture to phone
			File path = Environment
					.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

			String name = Data.ARTIST + "_"
					+ Data.PIC_NAMES[currentImageToSave] + ".jpeg";

			File file = new File(path, name);
			InputStream is = null;

			// check for file in directory
			if (file.exists()) {
				common.showToast("Photo Exists Already!");
			} else {
				try {
					boolean b1 = path.mkdirs();
					boolean b2 = path.exists();
					// Make sure the Pictures directory exists.
					if (b1 || b2) {

						// get file into input stream
						is = common.res
								.openRawResource(Data.PICS[currentImageToSave]);

						OutputStream os = new FileOutputStream(file);
						byte[] data = new byte[is.available()];
						is.read(data);
						os.write(data);
						is.close();
						os.close();

						common.showToast("Photo saved");

						// Tell the media scanner about the new file so
						// that it is immediately available to the user.
						MediaScannerConnection
								.scanFile(
										common.context,
										new String[] { file.toString() },
										null,
										new MediaScannerConnection.OnScanCompletedListener() {
											@Override
											public void onScanCompleted(
													String path, Uri uri) {
												//
											}
										});
					} else {
						common.showToast("Could not make/access directory.");
					}
				} catch (IOException e) {
					common.showToast("Error making/accessing directory.");
				}
			}
		} else {
			common.showToast("Directory not available/writable.");
		}
	}
}