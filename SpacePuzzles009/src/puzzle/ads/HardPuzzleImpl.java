package puzzle.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.graphics.Bitmap;
import android.view.MotionEvent;

/**
 * 
 * A class with Hard implementation is a 4 x 4 grid.
 * 
 * @author Rick
 * 
 */
public class HardPuzzleImpl implements Puzzle {

	public int PIECES = 16;
	int bitmapWd4;
	int bitmapHd4;

	public int piecesComplete;
	int jumbledNumber;
	int index;
	boolean isRandom, newImageComplete;
	Date startPuzzle = new Date();
	Date stopPuzzle = new Date();
	long currPuzzleTime = 0;
	CommonVariables common = CommonVariables.getInstance();

	public HardPuzzleImpl() {
	}

	public void getNewImageLoadedScaledDivided(Thread thread) {
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {

				while (!newImageComplete) {
					// fill with all valid numbers
					if (common.imagesShown.isEmpty())
						for (int i = 0; i < Data.PICS.length; i++)
							common.imagesShown.add(i);

					// get new index value from remaining images
					index = common.rand.nextInt(common.imagesShown.size());
					// get the value at that index for new image
					common.currentPuzzleImagePosition = common.imagesShown
							.get(index);
					// remove from list to prevent duplicates
					common.imagesShown.remove(index);

					common.image = common.decodeSampledBitmapFromResource(
							common.res,
							Data.PICS[common.currentPuzzleImagePosition],
							common.screenW, common.screenH);
					common.image = Bitmap.createScaledBitmap(common.image,
							common.screenW, common.screenH, true);

					newImageComplete = divideBitmap();

					if (newImageComplete) {
						resetTimer();
						common.errorLoading = false;
						common.solved = false;
						common.imageReady = true;
						System.gc();
					} else {
						common.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	public boolean divideBitmap() {
		common.numberOfPieces = PIECES;

		common.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzlePieces[i] = new PuzzlePiece();

		common.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzleSlots[i] = new PuzzleSlot();

		common.slotOrder = new int[PIECES];

		// set natural order
		for (int i = 0; i < PIECES; i++)
			common.slotOrder[i] = i;

		isRandom = false;
		while (!isRandom) {
			List<Integer> list = new ArrayList<Integer>();
			for (int i : common.slotOrder) {
				list.add(i);
			}

			Collections.shuffle(list);
			for (int i = 0; i < list.size(); i++) {
				common.slotOrder[i] = list.get(i);
			}

			for (int i = 0; i < common.slotOrder.length; i++) {
				if (common.slotOrder[i] != i)
					isRandom = true;
			}

			// if (cv.slotOrder[0] == 0 && cv.slotOrder[1] == 1
			// && cv.slotOrder[2] == 2 && cv.slotOrder[3] == 3
			// && cv.slotOrder[4] == 4 && cv.slotOrder[5] == 5
			// && cv.slotOrder[6] == 6 && cv.slotOrder[7] == 7
			// && cv.slotOrder[8] == 8 && cv.slotOrder[9] == 9
			// && cv.slotOrder[10] == 10 && cv.slotOrder[11] == 11
			// && cv.slotOrder[12] == 12 && cv.slotOrder[13] == 13
			// && cv.slotOrder[14] == 14 && cv.slotOrder[15] == 15) {
			// isRandom = false;
			// }
		}

		// re do if the image didn't split correctly
		common.imageSplit = false;
		piecesComplete = 0;

		while (!common.imageSplit) {
			int w = common.image.getWidth();
			int h = common.image.getHeight();
			bitmapWd4 = w / 4;
			bitmapHd4 = h / 4;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 4) {
					y = 0;
				} else if (i < 8) {
					y = bitmapHd4;
				} else if (i < 12) {
					y = bitmapHd4 * 2;
				} else {
					y = bitmapHd4 * 3;
				}

				x = (i % 4) * bitmapWd4;

				if (common.puzzlePieces[i].bitmap != null)
					common.puzzlePieces[i].bitmap.recycle();

				common.puzzlePieces[i].bitmap = null;
				common.puzzlePieces[i].bitmap = Bitmap.createBitmap(
						common.image, x, y, bitmapWd4, bitmapHd4);

				common.puzzlePieces[i].px = x;
				common.puzzlePieces[i].px2 = x + bitmapWd4;

				common.puzzlePieces[i].py = y;
				common.puzzlePieces[i].py2 = y + bitmapHd4;

				common.puzzleSlots[i].sx = x;
				common.puzzleSlots[i].sx2 = x + bitmapWd4;

				common.puzzleSlots[i].sy = y;
				common.puzzleSlots[i].sy2 = y + bitmapHd4;

				common.puzzleSlots[i].puzzlePiece = common.puzzlePieces[i];
				common.puzzleSlots[i].slotNum = common.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			common.imageSplit = true;
			common.image.recycle();
			common.image = null;
		}

		common.jumblePicture();

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean divideBitmapFromPreviousPuzzle() {
		common.numberOfPieces = PIECES;

		common.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzlePieces[i] = new PuzzlePiece();

		common.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzleSlots[i] = new PuzzleSlot();

		// re do if the image didn't split correctly
		common.imageSplit = false;
		piecesComplete = 0;

		while (!common.imageSplit) {
			int w = common.image.getWidth();
			int h = common.image.getHeight();
			bitmapWd4 = w / 4;
			bitmapHd4 = h / 4;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 4) {
					y = 0;
				} else if (i < 8) {
					y = bitmapHd4;
				} else if (i < 12) {
					y = bitmapHd4 * 2;
				} else {
					y = bitmapHd4 * 3;
				}

				x = (i % 4) * bitmapWd4;

				if (common.puzzlePieces[i].bitmap != null)
					common.puzzlePieces[i].bitmap.recycle();

				common.puzzlePieces[i].bitmap = null;
				common.puzzlePieces[i].bitmap = Bitmap.createBitmap(
						common.image, x, y, bitmapWd4, bitmapHd4);

				common.puzzlePieces[i].px = x;
				common.puzzlePieces[i].px2 = x + bitmapWd4;

				common.puzzlePieces[i].py = y;
				common.puzzlePieces[i].py2 = y + bitmapHd4;

				common.puzzleSlots[i].sx = x;
				common.puzzleSlots[i].sx2 = x + bitmapWd4;

				common.puzzleSlots[i].sy = y;
				common.puzzleSlots[i].sy2 = y + bitmapHd4;

				// cv.puzzleSlots[i].puzzlePiece = cv.puzzlePieces[i];
				common.puzzleSlots[i].slotNum = common.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			common.imageSplit = true;
			common.image.recycle();
			common.image = null;
		}

		// set slot by creating separate puzzle pieces and reassign individually
		boolean correctlyReassembled = false;
		while (!correctlyReassembled) {
			// use saved slot list to sort
			for (int toSlot = 0; toSlot < common.slotOrder.length; toSlot++) {

				// get new slot to take piece from and place into correct slot
				int fromSlot = common.slotOrder[toSlot];
				PuzzlePiece pieceA = common.puzzlePieces[fromSlot];

				common.puzzleSlots[toSlot].puzzlePiece = pieceA;
				common.puzzleSlots[toSlot].puzzlePiece.px = common.puzzleSlots[toSlot].sx;
				common.puzzleSlots[toSlot].puzzlePiece.py = common.puzzleSlots[toSlot].sy;
				common.puzzleSlots[toSlot].puzzlePiece.px2 = common.puzzleSlots[toSlot].sx2;
				common.puzzleSlots[toSlot].puzzlePiece.py2 = common.puzzleSlots[toSlot].sy2;
				common.puzzleSlots[toSlot].puzzlePiece.pieceNum = fromSlot;
			}

			correctlyReassembled = true;
			for (int j = 0; j < common.puzzleSlots.length; j++) {
				int a = common.puzzleSlots[j].puzzlePiece.pieceNum;
				int b = common.slotOrder[j];
				if (a != b) {
					correctlyReassembled = false;
				}
			}
		}

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int newx = (int) event.getX();
		int newy = (int) event.getY();

		// find the piece that was pressed down onto
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			common.movingPiece = false;
			if (newx < bitmapWd4) {
				// check first row
				if (newy < bitmapHd4) {
					// piece1 pressed on
					common.currPieceOnTouch = 0;
				} else if (newy < bitmapHd4 * 2) {
					// piece4 pressed on
					common.currPieceOnTouch = 4;
				} else if (newy < bitmapHd4 * 3) {
					// piece8 pressed on
					common.currPieceOnTouch = 8;
				} else {
					// piece12 pressed on
					common.currPieceOnTouch = 12;
				}
			} else if (newx < bitmapWd4 * 2) {
				// check second row
				if (newy < bitmapHd4) {
					// piece1 pressed on
					common.currPieceOnTouch = 1;
				} else if (newy < bitmapHd4 * 2) {
					// piece5 pressed on
					common.currPieceOnTouch = 5;
				} else if (newy < bitmapHd4 * 3) {
					// piece9 pressed on
					common.currPieceOnTouch = 9;
				} else {
					// piece13 pressed on
					common.currPieceOnTouch = 13;
				}
			} else if (newx < bitmapWd4 * 3) {
				// check third row
				if (newy < bitmapHd4) {
					// piece2 pressed on
					common.currPieceOnTouch = 2;
				} else if (newy < bitmapHd4 * 2) {
					// piece6 pressed on
					common.currPieceOnTouch = 6;
				} else if (newy < bitmapHd4 * 3) {
					// piece10 pressed on
					common.currPieceOnTouch = 10;
				} else {
					// piece14 pressed on
					common.currPieceOnTouch = 14;
				}
			} else if (newx < bitmapWd4 * 4) {
				// check fourth row
				if (newy < bitmapHd4) {
					// piece3 pressed on
					common.currPieceOnTouch = 3;
				} else if (newy < bitmapHd4 * 2) {
					// piece7 pressed on
					common.currPieceOnTouch = 7;
				} else if (newy < bitmapHd4 * 3) {
					// piece11 pressed on
					common.currPieceOnTouch = 11;
				} else {
					// piece15 pressed on
					common.currPieceOnTouch = 15;
				}
			}
		}

		if (event.getAction() == MotionEvent.ACTION_UP) {
			common.movingPiece = false;
			if (newx < bitmapWd4) {
				// check first row
				if (newy < bitmapHd4) {
					// piece1 pressed on
					common.currSlotOnTouchUp = 0;
				} else if (newy < bitmapHd4 * 2) {
					// piece4 pressed on
					common.currSlotOnTouchUp = 4;
				} else if (newy < bitmapHd4 * 3) {
					// piece8 pressed on
					common.currSlotOnTouchUp = 8;
				} else {
					// piece12 pressed on
					common.currSlotOnTouchUp = 12;
				}
			} else if (newx < bitmapWd4 * 2) {
				// check second row
				if (newy < bitmapHd4) {
					// piece1 pressed on
					common.currSlotOnTouchUp = 1;
				} else if (newy < bitmapHd4 * 2) {
					// piece5 pressed on
					common.currSlotOnTouchUp = 5;
				} else if (newy < bitmapHd4 * 3) {
					// piece9 pressed on
					common.currSlotOnTouchUp = 9;
				} else {
					// piece13 pressed on
					common.currSlotOnTouchUp = 13;
				}
			} else if (newx < bitmapWd4 * 3) {
				// check third row
				if (newy < bitmapHd4) {
					// piece2 pressed on
					common.currSlotOnTouchUp = 2;
				} else if (newy < bitmapHd4 * 2) {
					// piece6 pressed on
					common.currSlotOnTouchUp = 6;
				} else if (newy < bitmapHd4 * 3) {
					// piece10 pressed on
					common.currSlotOnTouchUp = 10;
				} else {
					// piece14 pressed on
					common.currSlotOnTouchUp = 14;
				}
			} else if (newx < bitmapWd4 * 4) {
				// check fourth row
				if (newy < bitmapHd4) {
					// piece3 pressed on
					common.currSlotOnTouchUp = 3;
				} else if (newy < bitmapHd4 * 2) {
					// piece7 pressed on
					common.currSlotOnTouchUp = 7;
				} else if (newy < bitmapHd4 * 3) {
					// piece11 pressed on
					common.currSlotOnTouchUp = 11;
				} else {
					// piece15 pressed on
					common.currSlotOnTouchUp = 15;
				}
			}

			// check for image to be in new slot
			if (common.currPieceOnTouch != common.currSlotOnTouchUp) {
				common.sendPieceToNewSlot(common.currPieceOnTouch,
						common.currSlotOnTouchUp);
				common.mySoundPool.playTapSound();
			} else {
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px = common.puzzleSlots[common.currSlotOnTouchUp].sx;
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py = common.puzzleSlots[common.currSlotOnTouchUp].sy;
			}
		}

		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			common.movingPiece = true;
			if (common.currPieceOnTouch >= 0
					|| common.currPieceOnTouch <= PIECES - 1) {
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px = newx
						- bitmapWd4 / 2;
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py = newy
						- bitmapHd4 / 2;
			}
		}

		common.inPlace = 0;
		for (int i = 0; i < common.numberOfPieces; i++) {
			if (common.puzzleSlots[i].slotNum == common.puzzleSlots[i].puzzlePiece.pieceNum) {
				common.inPlace++;
			}
		}

		if (common.inPlace == common.numberOfPieces) {
			stopTimer();
			common.solved = true;
			common.showToast(""
					+ Data.PIC_NAMES[common.currentPuzzleImagePosition]
					+ " solve Time: " + getSolveTime());
			return false;
		}

		return true;
	}

	@Override
	public void recylceAll() {
		if (common.image != null)
			common.image.recycle();

		for (int i = 0; i < common.puzzlePieces.length; i++)
			if (common.puzzlePieces != null)
				if (common.puzzlePieces[i] != null)
					if (common.puzzlePieces[i].bitmap != null)
						common.puzzlePieces[i].bitmap.recycle();

	}

	@Override
	public String getPercentComplete() {
		return "" + (float) piecesComplete / PIECES;
	}

	@Override
	public void initTimer() {
		startPuzzle = new Date();
	}

	@Override
	public void stopTimer() {
		stopPuzzle = new Date();
		currPuzzleTime += stopPuzzle.getTime() - startPuzzle.getTime();
	}

	@Override
	public void resetTimer() {
		currPuzzleTime = 0;
		startPuzzle = new Date();
	}

	@Override
	public double getSolveTime() {
		return currPuzzleTime / 1000.0;
	}

	@Override
	public int getCurrentImage() {
		// return current image
		return index;
	}

	@Override
	public void getPrevousImageLoadedScaledDivided(Thread thread) {
		// gets an image based on difficulty, image number and order from save
		// state
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {
				while (!newImageComplete) {
					// get new index value and then remove index
					index = common.currentPuzzleImagePosition;

					common.image = common.decodeSampledBitmapFromResource(
							common.res,
							Data.PICS[common.currentPuzzleImagePosition],
							common.screenW, common.screenH);

					common.image = Bitmap.createScaledBitmap(common.image,
							common.screenW, common.screenH, true);

					newImageComplete = divideBitmapFromPreviousPuzzle();

					if (newImageComplete) {
						resetTimer();
						common.errorLoading = false;
						common.solved = false;
						common.imageReady = true;
						System.gc();
					} else {
						common.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	@Override
	public void pause() {
		// stop timer for the current puzzle
		stopTimer();
	}
}