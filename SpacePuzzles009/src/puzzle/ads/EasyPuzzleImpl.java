package puzzle.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.android.gms.ads.AdRequest;

import android.graphics.Bitmap;
import android.view.MotionEvent;

/**
 * 
 * A class with Easy implementation as a 3 x 3 grid.
 * 
 * @author Rick
 * 
 */
public class EasyPuzzleImpl implements Puzzle {

	public int PIECES = 9;
	int bitmapWd3;
	int bitmapHd3;

	public int piecesComplete;
	int jumbledNumber;
	int index;
	boolean isRandom, newImageComplete;
	Date startPuzzle = new Date();
	Date stopPuzzle = new Date();
	long currPuzzleTime = 0;
	CommonVariables common = CommonVariables.getInstance();

	public EasyPuzzleImpl() {

	}

	public void getNewImageLoadedScaledDivided(Thread thread) {
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {

				while (!newImageComplete) {
					// fill with all valid numbers, if empty refill
					if (common.imagesShown.isEmpty())
						for (int i = 0; i < Data.PICS.length; i++)
							common.imagesShown.add(i);

					// get new index value from remaining images
					index = common.rand.nextInt(common.imagesShown.size());
					// get the value at that index for new image
					common.currentPuzzleImagePosition = common.imagesShown
							.get(index);
					// remove from list to prevent duplicates
					common.imagesShown.remove(index);

					common.image = common.decodeSampledBitmapFromResource(
							common.res,
							Data.PICS[common.currentPuzzleImagePosition],
							common.screenW, common.screenH);
					common.image = Bitmap.createScaledBitmap(common.image,
							common.screenW, common.screenH, true);

					newImageComplete = divideBitmap();

					if (newImageComplete) {
						resetTimer();
						common.errorLoading = false;
						common.solved = false;
						common.imageReady = true;
						System.gc();
					} else {
						common.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	public boolean divideBitmap() {
		// set number of pieces in puzzle to default value
		common.numberOfPieces = PIECES;

		// natural order of pieces
		common.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzlePieces[i] = new PuzzlePiece();

		// natural order of slots
		common.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzleSlots[i] = new PuzzleSlot();

		// prepare slot order for saved puzzle slot ordering
		common.slotOrder = new int[PIECES];

		// set natural order
		for (int i = 0; i < PIECES; i++)
			common.slotOrder[i] = i;

		isRandom = false;
		while (!isRandom) {
			List<Integer> list = new ArrayList<Integer>();
			for (int i : common.slotOrder) {
				list.add(i);
			}

			Collections.shuffle(list);
			for (int i = 0; i < list.size(); i++) {
				common.slotOrder[i] = list.get(i);
			}

			for (int i = 0; i < common.slotOrder.length; i++) {
				if (common.slotOrder[i] != i)
					isRandom = true;
			}

			if (common.slotOrder[0] == 0 && common.slotOrder[1] == 1
					&& common.slotOrder[2] == 2 && common.slotOrder[3] == 3
					&& common.slotOrder[4] == 4 && common.slotOrder[5] == 5
					&& common.slotOrder[6] == 6 && common.slotOrder[7] == 7
					&& common.slotOrder[8] == 8) {
				isRandom = false;
			}
		}

		// re do if the image didn't split correctly
		common.imageSplit = false;
		piecesComplete = 0;

		while (!common.imageSplit) {
			int w = common.image.getWidth();
			int h = common.image.getHeight();
			bitmapWd3 = w / 3;
			bitmapHd3 = h / 3;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 3) {
					y = 0;
				} else if (i < 6) {
					y = bitmapHd3;
				} else {
					y = bitmapHd3 * 2;
				}

				x = (i % 3) * bitmapWd3;

				if (common.puzzlePieces[i].bitmap != null)
					common.puzzlePieces[i].bitmap.recycle();

				common.puzzlePieces[i].bitmap = null;
				common.puzzlePieces[i].bitmap = Bitmap.createBitmap(
						common.image, x, y, bitmapWd3, bitmapHd3);

				common.puzzlePieces[i].px = x;
				common.puzzlePieces[i].px2 = x + bitmapWd3;

				common.puzzlePieces[i].py = y;
				common.puzzlePieces[i].py2 = y + bitmapHd3;

				common.puzzleSlots[i].sx = x;
				common.puzzleSlots[i].sx2 = x + bitmapWd3;

				common.puzzleSlots[i].sy = y;
				common.puzzleSlots[i].sy2 = y + bitmapHd3;

				common.puzzleSlots[i].puzzlePiece = common.puzzlePieces[i];
				common.puzzleSlots[i].slotNum = common.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			common.imageSplit = true;
			common.image.recycle();
			common.image = null;
		}

		common.jumblePicture();

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean divideBitmapFromPreviousPuzzle() {
		common.numberOfPieces = PIECES;

		common.puzzlePieces = new PuzzlePiece[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzlePieces[i] = new PuzzlePiece();

		common.puzzleSlots = new PuzzleSlot[PIECES];
		for (int i = 0; i < common.numberOfPieces; i++)
			common.puzzleSlots[i] = new PuzzleSlot();

		// re do if the image didn't split correctly
		common.imageSplit = false;
		piecesComplete = 0;

		while (!common.imageSplit) {
			int w = common.image.getWidth();
			int h = common.image.getHeight();
			bitmapWd3 = w / 3;
			bitmapHd3 = h / 3;
			int x, y;
			for (int i = 0; i < PIECES; i++) {
				if (i < 3) {
					y = 0;
				} else if (i < 6) {
					y = bitmapHd3;
				} else {
					y = bitmapHd3 * 2;
				}

				x = (i % 3) * bitmapWd3;

				if (common.puzzlePieces[i].bitmap != null)
					common.puzzlePieces[i].bitmap.recycle();

				// create bitmap section
				common.puzzlePieces[i].bitmap = null;
				common.puzzlePieces[i].bitmap = Bitmap.createBitmap(
						common.image, x, y, bitmapWd3, bitmapHd3);

				// set default slot values
				common.puzzlePieces[i].px = x;
				common.puzzlePieces[i].px2 = x + bitmapWd3;

				common.puzzlePieces[i].py = y;
				common.puzzlePieces[i].py2 = y + bitmapHd3;

				common.puzzleSlots[i].sx = x;
				common.puzzleSlots[i].sx2 = x + bitmapWd3;

				common.puzzleSlots[i].sy = y;
				common.puzzleSlots[i].sy2 = y + bitmapHd3;

				common.puzzleSlots[i].slotNum = common.puzzleSlots[i].puzzlePiece.pieceNum = i;

				piecesComplete++;

			}
			common.imageSplit = true;
			common.image.recycle();
			common.image = null;
		}

		// set slot by creating separate puzzle pieces and reassign individually
		boolean correctlyReassembled = false;
		while (!correctlyReassembled) {
			// use saved slot list to sort
			for (int toSlot = 0; toSlot < common.slotOrder.length; toSlot++) {

				// get new slot to take piece from and place into correct slot
				int fromSlot = common.slotOrder[toSlot];
				PuzzlePiece pieceA = common.puzzlePieces[fromSlot];

				common.puzzleSlots[toSlot].puzzlePiece = pieceA;
				common.puzzleSlots[toSlot].puzzlePiece.px = common.puzzleSlots[toSlot].sx;
				common.puzzleSlots[toSlot].puzzlePiece.py = common.puzzleSlots[toSlot].sy;
				common.puzzleSlots[toSlot].puzzlePiece.px2 = common.puzzleSlots[toSlot].sx2;
				common.puzzleSlots[toSlot].puzzlePiece.py2 = common.puzzleSlots[toSlot].sy2;
				common.puzzleSlots[toSlot].puzzlePiece.pieceNum = fromSlot;
			}

			correctlyReassembled = true;
			for (int j = 0; j < common.puzzleSlots.length; j++) {
				int a = common.puzzleSlots[j].puzzlePiece.pieceNum;
				int b = common.slotOrder[j];
				if (a != b) {
					correctlyReassembled = false;
				}
			}
		}

		if (piecesComplete == PIECES)
			return true;

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int newx = (int) event.getX();
		int newy = (int) event.getY();

		// find the piece that was pressed down onto
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			common.movingPiece = false;
			if (newx < bitmapWd3) {
				// check first row
				if (newy < bitmapHd3) {
					// piece1 pressed on
					common.currPieceOnTouch = 0;
				} else if (newy < bitmapHd3 * 2) {
					// piece2 pressed on
					common.currPieceOnTouch = 3;
				} else {
					// piece3 pressed on
					common.currPieceOnTouch = 6;
				}
			} else if (newx < bitmapWd3 * 2) {
				// check second row
				if (newy < bitmapHd3) {
					// piece4 pressed on
					common.currPieceOnTouch = 1;
				} else if (newy < bitmapHd3 * 2) {
					// piece5 pressed on
					common.currPieceOnTouch = 4;
				} else {
					// piece6 pressed on
					common.currPieceOnTouch = 7;
				}
			} else if (newx < bitmapWd3 * 3) {
				// check third row
				if (newy < bitmapHd3) {
					// piece7 pressed on
					common.currPieceOnTouch = 2;
				} else if (newy < bitmapHd3 * 2) {
					// piece8 pressed on
					common.currPieceOnTouch = 5;
				} else {
					// piece9 pressed on
					common.currPieceOnTouch = 8;
				}
			}
		}

		if (event.getAction() == MotionEvent.ACTION_UP) {
			common.movingPiece = false;
			if (newx < bitmapWd3) {
				// check first column
				if (newy < bitmapHd3) {
					// piece1 pressed on
					common.currSlotOnTouchUp = 0;
				} else if (newy < bitmapHd3 * 2) {
					// piece2 pressed on
					common.currSlotOnTouchUp = 3;
				} else {
					common.currSlotOnTouchUp = 6;
				}

			} else if (newx < bitmapWd3 * 2) {
				// check second column
				if (newy < bitmapHd3) {
					// piece4 pressed on
					common.currSlotOnTouchUp = 1;
				} else if (newy < bitmapHd3 * 2) {
					// piece5 pressed on
					common.currSlotOnTouchUp = 4;
				} else {
					// piece6 pressed on
					common.currSlotOnTouchUp = 7;
				}
			} else if (newx < bitmapWd3 * 3) {
				// check third column
				if (newy < bitmapHd3) {
					// piece7 pressed on
					common.currSlotOnTouchUp = 2;
				} else if (newy < bitmapHd3 * 2) {
					// piece8 pressed on
					common.currSlotOnTouchUp = 5;
				} else {
					// piece9 pressed on
					common.currSlotOnTouchUp = 8;
				}
			}

			// check for image to be in new slot
			if (common.currPieceOnTouch != common.currSlotOnTouchUp) {
				common.sendPieceToNewSlot(common.currPieceOnTouch,
						common.currSlotOnTouchUp);
				common.mySoundPool.playTapSound();
			} else {
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px = common.puzzleSlots[common.currSlotOnTouchUp].sx;
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py = common.puzzleSlots[common.currSlotOnTouchUp].sy;
			}

		}

		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			common.movingPiece = true;
			if (common.currPieceOnTouch >= 0 || common.currPieceOnTouch <= 8) {
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px = newx
						- bitmapWd3 / 2;
				common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py = newy
						- bitmapHd3 / 2;
			}
		}

		// begin to check for pieces to be arranged correctly
		common.inPlace = 0;
		for (int i = 0; i < common.numberOfPieces; i++) {
			if (common.puzzleSlots[i].slotNum == common.puzzleSlots[i].puzzlePiece.pieceNum) {
				common.inPlace++;
			}
		}

		if (common.inPlace == common.numberOfPieces) {
			stopTimer();
			common.solved = true;
			common.showToast(""
					+ Data.PIC_NAMES[common.currentPuzzleImagePosition]
					+ " solve Time: " + getSolveTime());
			return false;
		}

		return true;
	}

	@Override
	public void recylceAll() {
		if (common.image != null)
			common.image.recycle();

		for (int i = 0; i < common.puzzlePieces.length; i++)
			if (common.puzzlePieces != null)
				if (common.puzzlePieces[i] != null)
					if (common.puzzlePieces[i].bitmap != null)
						common.puzzlePieces[i].bitmap.recycle();

	}

	@Override
	public String getPercentComplete() {
		return "" + (float) piecesComplete / PIECES;
	}

	@Override
	public void initTimer() {
		startPuzzle = new Date();
	}

	@Override
	public void stopTimer() {
		stopPuzzle = new Date();
		currPuzzleTime += stopPuzzle.getTime() - startPuzzle.getTime();
	}

	@Override
	public void resetTimer() {
		currPuzzleTime = 0;
		startPuzzle = new Date();
	}

	@Override
	public double getSolveTime() {
		return currPuzzleTime / 1000.0;
	}

	@Override
	public int getCurrentImage() {
		// get current image number for save state
		return index;
	}

	@Override
	public void getPrevousImageLoadedScaledDivided(Thread thread) {
		// gets an image based on difficulty, image number and order from save
		// state
		newImageComplete = false;
		if (thread != null && thread.isAlive())
			thread.interrupt();

		thread = new Thread() {
			@Override
			public void run() {

				while (!newImageComplete) {
					// get new index value and then remove index
					index = common.currentPuzzleImagePosition;

					common.image = common.decodeSampledBitmapFromResource(
							common.res,
							Data.PICS[common.currentPuzzleImagePosition],
							common.screenW, common.screenH);

					common.image = Bitmap.createScaledBitmap(common.image,
							common.screenW, common.screenH, true);

					newImageComplete = divideBitmapFromPreviousPuzzle();

					if (newImageComplete) {
						resetTimer();
						common.errorLoading = false;
						common.solved = false;
						common.imageReady = true;
						System.gc();
					} else {
						common.errorLoading = true;
					}
				}
			}
		};
		thread.start();
	}

	@Override
	public void pause() {
		stopTimer();
	}
}