package puzzle.ads;

import nasa.puzzles.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 
 * Class that will be the object to be drawn on, contains the thread to draw and
 * update.
 * 
 * @author Rick
 *
 */
public class PuzzleSurface extends SurfaceView implements
		SurfaceHolder.Callback, View.OnTouchListener {

	// game modes
	public static final int STATE_RESUMING = 0;
	public static final int STATE_LOSE = 1;
	public static final int STATE_PAUSE = 2;
	public static final int STATE_READY = 3;
	public static final int STATE_RUNNING = 4;

	// difficulty
	public static final int EASY = 0;
	public static final int HARD = 1;
	public static final int VERY_HARD = 2;

	// draw values
	public static final int TRANS_VALUE = (255 / 2);
	public static final int STROKE_VALUE = 5;

	Thread loadingThread;

	CommonVariables common = CommonVariables.getInstance();
	PuzzleThread puzzleThread;
	PuzzleFactory pf = new PuzzleFactory();
	Puzzle puzzle;
	Handler myHandler;
	Paint borderPaintA, borderPaintB, transPaint, fullPaint;
	Toast toast;

	boolean difficultyChanged;
	public MyMediaPlayer myMediaPlayer;

	class IncomingHandlerCallback implements Handler.Callback {
		@Override
		public boolean handleMessage(Message m) {
			// handle message code
			common.mStatusText.setVisibility(m.getData().getInt("viz"));
			common.mStatusText.setText(m.getData().getString("text"));
			return true;
		}
	}

	public PuzzleSurface(Context context, AttributeSet attrs) {
		super(context, attrs);
		// register for call backs
		// add context and resources shared to variables class
		common.context = context;
		common.res = context.getResources();

		SurfaceHolder holder = getHolder();
		holder.addCallback(this);

		// prepare paint for drawing
		borderPaintA = new Paint();
		borderPaintA.setStyle(Paint.Style.STROKE);
		borderPaintA.setStrokeWidth(STROKE_VALUE);
		borderPaintA.setColor(Color.LTGRAY);
		borderPaintA.setAlpha(TRANS_VALUE);

		borderPaintB = new Paint();
		borderPaintB.setStyle(Paint.Style.STROKE);
		borderPaintB.setStrokeWidth(STROKE_VALUE);
		borderPaintB.setColor(Color.DKGRAY);
		borderPaintB.setAlpha(TRANS_VALUE);

		transPaint = new Paint();
		transPaint.setAlpha(TRANS_VALUE);
		transPaint.setStyle(Paint.Style.FILL);

		fullPaint = new Paint();
		myHandler = new Handler(new IncomingHandlerCallback());

		// create the thread and use the handler to send messages back
		// puzzleThread = new PuzzleThread(holder, context, myHandler);
	}

	public void showButtons() {
		if (common.mNextButton.getVisibility() == View.INVISIBLE)
			common.mNextButton.setVisibility(VISIBLE);
		if (common.rightWebLinkButton.getVisibility() == View.INVISIBLE)
			common.rightWebLinkButton.setVisibility(VISIBLE);
		if (common.leftWebLinkButton.getVisibility() == View.INVISIBLE)
			common.leftWebLinkButton.setVisibility(VISIBLE);
		if (common.adView.getVisibility() == View.INVISIBLE)
			common.adView.setVisibility(VISIBLE);
	}

	public void hideButtons() {
		if (common.mNextButton.getVisibility() == View.VISIBLE)
			common.mNextButton.setVisibility(INVISIBLE);
		if (common.rightWebLinkButton.getVisibility() == View.VISIBLE)
			common.rightWebLinkButton.setVisibility(INVISIBLE);
		if (common.leftWebLinkButton.getVisibility() == View.VISIBLE)
			common.leftWebLinkButton.setVisibility(INVISIBLE);
		if (common.adView.getVisibility() == View.VISIBLE) {
			common.adView.setVisibility(INVISIBLE);
		}
	}

	public void showToast(Context cont, String message) {
		// create if not, or set text to it
		if (toast == null) {
			toast = Toast.makeText(cont, message, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
		}
		if (!toast.getView().isShown()) {
			toast.setText(message);
			toast.show();
		} else {
			toast.setText(message);
		}
	}

	public void setButton(Button button) {
		common.mNextButton = button;
	}

	public void setBottomRightButton(ImageButton daButton) {
		common.rightWebLinkButton = daButton;
	}

	public void setBottomLeftButton(ImageButton faButton) {
		common.leftWebLinkButton = faButton;
	}

	public void setTextView(TextView textView) {
		common.mStatusText = textView;
	}

	public void onPause() {
		puzzleThread.setState(STATE_PAUSE);
		puzzleThread.setRunning(false);
	}

	public void onResume() {
		puzzleThread.setState(STATE_RUNNING);
		puzzleThread.setRunning(true);
	}

	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		if (!hasWindowFocus) {
			puzzleThread.pause();
		}
	}

	/**
	 * called after any structural changes (format or size) have been made to
	 * the surface.
	 */
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		puzzleThread.setSurfaceSize(width, height);
	}

	/**
	 * This is called immediately after the surface is first created.
	 */
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d("PuzzleSurface", "surfaceCreated start");
		// check for the state of the thread

		Log.d("PuzzleSurface", "surfaceCreated end");
	}

	/**
	 * This is called immediately before home or back and a surface is being
	 * destroyed.
	 */
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d("PuzzleSurface", "surfaceDestroyed start");
		boolean retry = true;
		puzzleThread.setRunning(false);
		while (retry) {
			try {
				puzzleThread.join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
		Log.d("PuzzleSurface", "surfaceDestroyed end");
	}

	@Override
	public boolean performClick() {
		super.performClick();
		return true;
	}

	/**
	 * 
	 */
	@Override
	public boolean onTouch(View view, MotionEvent event) {
		synchronized (puzzleThread.getSurfaceHolder()) {
			// implement performClick
			switch (puzzleThread.mMode) {
			case STATE_READY:
				hideButtons();
				puzzleThread.doStart();
				startTimer();
				return false;
			case STATE_PAUSE:
			case STATE_RESUMING:
				if (common.mStatusText.isShown()) {
					common.mStatusText.setVisibility(View.INVISIBLE);
					hideButtons();
				}
				puzzleThread.doStart();
				return false;
			case STATE_RUNNING:
				if (common.solved) {
					// win screen is before the next image button is pressed
					if (common.mNextButton.getVisibility() == View.VISIBLE)
						common.mNextButton.setVisibility(INVISIBLE);
					else
						common.mNextButton.setVisibility(VISIBLE);

					if (common.rightWebLinkButton.getVisibility() == View.VISIBLE)
						common.rightWebLinkButton.setVisibility(INVISIBLE);
					else
						common.rightWebLinkButton.setVisibility(VISIBLE);

					if (common.leftWebLinkButton.getVisibility() == View.VISIBLE)
						common.leftWebLinkButton.setVisibility(INVISIBLE);
					else
						common.leftWebLinkButton.setVisibility(VISIBLE);

					if (common.adView.getVisibility() == View.VISIBLE)
						common.adView.setVisibility(INVISIBLE);
					else
						common.adView.setVisibility(VISIBLE);

					return false;
				} else if (common.imageReady) {
					return puzzle.onTouchEvent(event);
				}
				break;
			}
			return true;
		}
	}

	public void setCurrPieceOnTouch(int newPiece) {
		common.currPieceOnTouch = newPiece;
	}

	public void recreatePuzzle() {
		// to recreate hide the UI
		common.mySoundPool.playChimeSound();
		hideButtons();
		puzzleThread.setState(STATE_PAUSE);
		puzzle = pf.getPuzzle(common.difficulty);
		common.imageReady = false;
		puzzle.getNewImageLoadedScaledDivided(loadingThread);
	}

	public void planetActivity() {
		Intent intent1 = new Intent(Intent.ACTION_VIEW);
		intent1.setData(Uri
				.parse(Data.PIC_URLS[common.currentPuzzleImagePosition]));
		common.context.startActivity(intent1);
	}

	public void musicLinkActivity() {
		Intent intent2 = new Intent(Intent.ACTION_VIEW);
		intent2.setData(Uri.parse(Data.MUSIC_LINK));
		common.context.startActivity(intent2);
	}

	public void cleanUp() {
		if (puzzle != null)
			puzzle.recylceAll();
		if (loadingThread != null && loadingThread.isAlive()) {
			loadingThread.interrupt();
			loadingThread = null;
		}
	}

	public void startTimer() {
		// start timer for new puzzle in puzzle
		if (puzzle != null)
			puzzle.initTimer();
	}

	public void pause() {
		// add conditional null checks here
		if (puzzleThread != null)
			puzzleThread.pause();
	}

	public void resume() {
		startTimer();
	}

	public void jumblePictureFromOldPuzzle() {
		for (int i = 0; i < common.numberOfPieces; i++) {
			int oldslot = i;
			int newslot = common.slotOrder[i];
			if (oldslot != newslot) {
				PuzzlePiece temp = new PuzzlePiece();
				temp = common.puzzleSlots[oldslot].puzzlePiece;
				common.puzzleSlots[oldslot].puzzlePiece = common.puzzleSlots[newslot].puzzlePiece;
				common.puzzleSlots[oldslot].puzzlePiece.px = common.puzzleSlots[oldslot].sx;
				common.puzzleSlots[oldslot].puzzlePiece.py = common.puzzleSlots[oldslot].sy;
				common.puzzleSlots[newslot].puzzlePiece = temp;
				common.puzzleSlots[newslot].puzzlePiece.px = common.puzzleSlots[newslot].sx;
				common.puzzleSlots[newslot].puzzlePiece.py = common.puzzleSlots[newslot].sy;
				temp = null;
			}
		}
	}

	public class PuzzleThread extends Thread {

		boolean mRun;
		SurfaceHolder mSurfaceHolder;
		public int mMode;
		public Handler mHandler;

		/**
		 * Locking object for the thread.
		 */
		Object mRunLock = new Object();

		public PuzzleThread(SurfaceHolder surfaceHolder, Context context,
				Handler handler) {
			// get handles to some important objects such as the context
			// which
			// will be updated to continue the thread
			mSurfaceHolder = surfaceHolder;
			mHandler = handler;
			common.context = context;
		}

		@Override
		public void run() {
			Log.d("PuzzleSurface", "run start");
			while (mRun) {
				Canvas c = null;
				try {
					c = mSurfaceHolder.lockCanvas(null);
					synchronized (mSurfaceHolder) {
						if (mMode == STATE_RUNNING)
							updatePhysics();
						synchronized (mRunLock) {
							if (mRun)
								doDraw(c);
						}
					}
				} finally {
					if (c != null) {
						mSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
			Log.d("PuzzleSurface", "run start");
		}

		public void setRunning(boolean b) {
			// stop the thread from updating the graphics
			synchronized (mSurfaceHolder) {
				mRun = b;
			}
		}

		public void updatePhysics() {
			if (difficultyChanged) {
				difficultyChanged = false;
				recreatePuzzle();
			}
		}

		public void doDraw(Canvas canvas) {
			if (canvas != null) {
				if (puzzle == null) {
					Activity act = (Activity) common.context;
					act.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							common.adView.loadAd(common.adRequest);
						}
					});
					if (common.resumePreviousPuzzle) {
						puzzle = pf.getPuzzle(common.difficulty);
						common.imageReady = false;
						puzzle.getPrevousImageLoadedScaledDivided(loadingThread);
						puzzle.initTimer();
						common.mySoundPool.playChimeSound();
					} else if (difficultyChanged) {
						difficultyChanged = false;
						recreatePuzzle();
					} else {
						// start with an easy puzzle
						puzzle = pf.getPuzzle(EASY);
						common.imageReady = false;
						puzzle.getNewImageLoadedScaledDivided(loadingThread);
						puzzle.initTimer();
						common.mySoundPool.playChimeSound();
					}
				} else if (common.imageReady) {

					canvas.drawColor(Color.BLACK);

					// draw a moving piece away from its original location
					if (common.movingPiece) {
						for (int i = 0; i < common.numberOfPieces; i++) {
							// draw pieces
							if (!common.puzzleSlots[i].puzzlePiece.bitmap
									.isRecycled()
									&& common.currPieceOnTouch != i)
								canvas.drawBitmap(
										common.puzzleSlots[i].puzzlePiece.bitmap,
										common.puzzleSlots[i].puzzlePiece.px,
										common.puzzleSlots[i].puzzlePiece.py,
										null);
							// draw border to pieces
							if (!common.solved && common.drawBorders)
								canvas.drawRect(
										common.puzzleSlots[i].sx,
										common.puzzleSlots[i].sy,
										common.puzzleSlots[i].sx
												+ common.puzzleSlots[i].puzzlePiece.bitmap
														.getWidth(),
										common.puzzleSlots[i].sy
												+ common.puzzleSlots[i].puzzlePiece.bitmap
														.getHeight(),
										borderPaintA);
						}

						// draw the actual piece
						if (!common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.bitmap
								.isRecycled()) {

							// draw moving image in original location
							canvas.drawBitmap(
									common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.bitmap,
									common.puzzleSlots[common.currPieceOnTouch].sx,
									common.puzzleSlots[common.currPieceOnTouch].sy,
									transPaint);

							// draw border around original piece location
							canvas.drawRect(
									common.puzzleSlots[common.currPieceOnTouch].sx,
									common.puzzleSlots[common.currPieceOnTouch].sy,
									common.puzzleSlots[common.currPieceOnTouch].sx
											+ common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.bitmap
													.getWidth(),
									common.puzzleSlots[common.currPieceOnTouch].sy
											+ common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.bitmap
													.getHeight(), borderPaintB);

							// draw moving piece
							canvas.drawBitmap(
									common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.bitmap,
									common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px,
									common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py,
									fullPaint);

							// draw border around moving piece
							if (common.drawBorders)
								canvas.drawRect(
										common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px
												+ (STROKE_VALUE / 2),
										common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py
												+ (STROKE_VALUE / 2),
										common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.px
												+ common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.bitmap
														.getWidth()
												- (STROKE_VALUE / 2),
										common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.py
												+ common.puzzleSlots[common.currPieceOnTouch].puzzlePiece.bitmap
														.getHeight()
												- (STROKE_VALUE / 2),
										borderPaintA);
						}
					} else {
						for (int i = 0; i < common.numberOfPieces; i++) {
							if (!common.puzzleSlots[i].puzzlePiece.bitmap
									.isRecycled()) {
								// draw pieces
								canvas.drawBitmap(
										common.puzzleSlots[i].puzzlePiece.bitmap,
										common.puzzleSlots[i].puzzlePiece.px,
										common.puzzleSlots[i].puzzlePiece.py,
										null);
								// draw borders
								if (!common.solved && common.drawBorders)
									canvas.drawRect(
											common.puzzleSlots[i].sx,
											common.puzzleSlots[i].sy,
											common.puzzleSlots[i].sx
													+ common.puzzleSlots[i].puzzlePiece.bitmap
															.getWidth(),
											common.puzzleSlots[i].sy
													+ common.puzzleSlots[i].puzzlePiece.bitmap
															.getHeight(),
											borderPaintA);
							}
						}
					}
				} else {
					// the image is loading
					if (common.errorLoading) {
						canvas.drawColor(Color.BLUE);
					} else {
						canvas.drawColor(Color.RED);
					}
				}
			}
		}

		public void toggleSetSound() {
			synchronized (mSurfaceHolder) {
				if (common.playTapSound) {
					common.playTapSound = false;
					showToast(common.context, "Set Effect Off");
				} else {
					common.playTapSound = true;
					showToast(common.context, "Set Effect On");
				}
			}
		}

		public void toggleBorder() {
			synchronized (mSurfaceHolder) {
				if (common.drawBorders) {
					common.drawBorders = false;
					showToast(common.context, "Borders Off");
				} else {
					common.drawBorders = true;
					showToast(common.context, "Borders On");
				}
			}
		}

		public void toggleWinSound() {
			synchronized (mSurfaceHolder) {
				if (common.playChimeSound) {
					common.playChimeSound = false;
					showToast(common.context, "Win Effect Off");
				} else {
					common.playChimeSound = true;
					showToast(common.context, "Win Effect On");
				}
			}
		}

		public void toggleMusic() {
			synchronized (mSurfaceHolder) {
				if (common.playMusic) {
					common.playMusic = false;
					myMediaPlayer.pause();
					showToast(common.context, "Music Off");
				} else {
					common.playMusic = true;
					myMediaPlayer.resume();
					showToast(common.context, "Music On");
				}
			}
		}

		public void setState(int mode) {
			synchronized (mSurfaceHolder) {
				setState(mode, null);
			}
		}

		public void setState(int mode, CharSequence message) {
			synchronized (mSurfaceHolder) {
				mMode = mode;

				if (mMode == STATE_RUNNING) {
					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("text", "");
					b.putInt("viz", View.INVISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
				} else {
					Resources res = common.context.getResources();
					CharSequence str = "";
					if (mMode == STATE_READY)
						str = res.getText(R.string.mode_ready);
					else if (mMode == STATE_PAUSE)
						str = res.getText(R.string.mode_pause);
					else if (mMode == STATE_LOSE)
						str = res.getText(R.string.mode_lose);

					if (message != null) {
						str = message + "\n" + str;
					}

					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("text", str.toString());
					b.putInt("viz", View.VISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
				}
			}
		}

		public void setSurfaceSize(int width, int height) {
			synchronized (mSurfaceHolder) {
				common.screenW = width;
				common.screenH = height;
			}
		}

		public void doStart() {
			synchronized (mSurfaceHolder) {
				// First set the game for Medium difficulty

				// Adjust difficulty for EASY/HARD
				if (common.difficulty == EASY) {

				} else if (common.difficulty == HARD) {

				} else if (common.difficulty == VERY_HARD) {

				}

				setState(STATE_RUNNING);
			}
		}

		public void pause() {
			synchronized (mSurfaceHolder) {
				setState(STATE_PAUSE);
			}
		}

		public void setDifficulty(int diff) {
			synchronized (mSurfaceHolder) {
				if (!common.imageReady)
					showToast(common.context,
							"Please wait for current image to load first.");
				else {
					if (diff != common.difficulty) {
						// show toast of new difficulty
						if (diff == 0)
							showToast(common.context, "Easy 3 x 3");
						else if (diff == 1)
							showToast(common.context, "Hard 4 x 4");
						else if (diff == 2)
							showToast(common.context, "Very Hard 5 x 5");
						common.difficulty = diff;
						// difficultyChanged = true;
						recreatePuzzle();
					}
				}
			}
		}

		public Bundle saveState(Bundle map) {
			synchronized (mSurfaceHolder) {
				if (map != null) {
					// map.putInt("difficulty", cv.difficulty);
				}
			}
			return map;
		}

		public SurfaceHolder getSurfaceHolder() {
			return mSurfaceHolder;
		}

	}

	public void onStart() {
		if (puzzleThread != null) {
			switch (puzzleThread.getState()) {
			case BLOCKED:
				// The thread is waiting.
				break;
			case NEW:
				// The thread may be run.
				puzzleThread.start();
				break;
			case RUNNABLE:
				// The thread is blocked and waiting for a lock.
				break;
			case TERMINATED:
				puzzleThread = new PuzzleThread(getHolder(), getContext(),
						myHandler);
				puzzleThread.start();
				break;
			case TIMED_WAITING:
				// The thread has been terminated.
				break;
			case WAITING:
				// The thread is waiting for a specified amount of time.
				break;
			default:
				break;
			}
		} else {
			puzzleThread = new PuzzleThread(getHolder(), getContext(),
					myHandler);
			puzzleThread.start();
		}
		puzzleThread.setRunning(true);
	}

	public void nextImage() {
		hideButtons();
		common.mySoundPool.playChimeSound();
		puzzleThread.setState(PuzzleSurface.STATE_PAUSE);
		common.imageReady = false;
		puzzle.getNewImageLoadedScaledDivided(loadingThread);
	}

}