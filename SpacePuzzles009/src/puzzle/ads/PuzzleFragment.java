package puzzle.ads;

import nasa.puzzles.R;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * A placeholder fragment containing a simple view.
 */
public class PuzzleFragment extends Fragment {

	PuzzleSurface puzzleSurface;
	MySoundPool mySoundPool;
	AudioManager audioManager;
	MyMediaPlayer myMediaPlayer = new MyMediaPlayer();
	CommonVariables common = CommonVariables.getInstance();
	AdView mAdView;
	AdRequest mAdRequest;

	public static final String MY_PREFERENCES = "MyPuzzle";
	private static final String COLUMN_DIFFICULTY = "DIFFICULTY";
	private static final String COLUMN_IMAGE_POSITION = "IMAGEPOSITION";
	private static final String COLUMN_SOUND_POSITION = "SOUNDPOSITION";
	private static final String COLUMN_SLOTS = "SLOTS";
	private static final String COLUMN_SOUND = "SOUND";
	private static final String COLUMN_CHIME = "CHIME";
	private static final String COLUMN_MUSIC = "MUSIC";
	private static final String COLUMN_BORDER = "BORDER";

	SharedPreferences sharedpreferences;

	public PuzzleFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_main, container, false);

		puzzleSurface = (PuzzleSurface) view.findViewById(R.id.puzzle);
		puzzleSurface.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				return puzzleSurface.onTouch(view, event);
			}
		});

		sharedpreferences = this.getActivity().getSharedPreferences(
				MY_PREFERENCES, Context.MODE_PRIVATE);

		// check for all to be loaded here
		int acc = 0;
		if (sharedpreferences.contains(COLUMN_IMAGE_POSITION)) {
			int temp = sharedpreferences.getInt(COLUMN_IMAGE_POSITION, 0);
			if (common.currentPuzzleImagePosition >= 0
					&& common.currentPuzzleImagePosition <= Data.PICS.length) {
				common.currentPuzzleImagePosition = temp;
				acc++;
			}
		}

		if (sharedpreferences.contains(COLUMN_DIFFICULTY)) {
			common.difficulty = sharedpreferences.getInt(COLUMN_DIFFICULTY, 0);
			if (common.difficulty == 0 || common.difficulty == 1
					|| common.difficulty == 2) {
				switch (common.difficulty) {
				case 0:
					if (common.currentPuzzleImagePosition >= 0
							|| common.currentPuzzleImagePosition <= 8)
						acc++;
					break;
				case 1:
					if (common.currentPuzzleImagePosition >= 0
							|| common.currentPuzzleImagePosition <= 15)
						acc++;
					break;
				case 2:
					if (common.currentPuzzleImagePosition >= 0
							|| common.currentPuzzleImagePosition <= 24)
						acc++;
					break;
				}
			}
		}

		if (sharedpreferences.contains(COLUMN_SLOTS)) {

			String temp = sharedpreferences.getString(COLUMN_SLOTS, ",");
			int tempInt = 0;
			String[] stringSlots = null;

			if (!temp.equals("")) {
				switch (common.difficulty) {
				case 0:
					tempInt = 9;
					stringSlots = temp.split(",");
					if (stringSlots.length == tempInt) {
						common.setSlots(temp);
						acc++;
					}
					break;
				case 1:
					tempInt = 16;
					stringSlots = temp.split(",");
					if (stringSlots.length == tempInt) {
						common.setSlots(temp);
						acc++;
					}
					break;
				case 2:
					tempInt = 25;
					stringSlots = temp.split(",");
					if (stringSlots.length == tempInt) {
						common.setSlots(temp);
						acc++;
					}
					break;
				}
			}
		}

		if (acc == 3) {
			common.resumePreviousPuzzle = true;
		}

		common.mStatusText = (TextView) view.findViewById(R.id.text);

		puzzleSurface.setButton((Button) view.findViewById(R.id.nextButton));
		common.mNextButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				puzzleSurface.nextImage();
			}
		});

		puzzleSurface.setBottomRightButton((ImageButton) view
				.findViewById(R.id.musiclinkbutton));
		common.rightWebLinkButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				puzzleSurface.musicLinkActivity();
			}
		});

		puzzleSurface.setBottomLeftButton((ImageButton) view
				.findViewById(R.id.planetlinkbutton));
		common.leftWebLinkButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				puzzleSurface.planetActivity();
			}
		});

		myMediaPlayer.init();
		puzzleSurface.myMediaPlayer = myMediaPlayer;

		mySoundPool = new MySoundPool(10, AudioManager.STREAM_MUSIC, 100);
		mySoundPool.init();
		common.mySoundPool = mySoundPool;

		mAdView = (AdView) view.findViewById(R.id.adView);
		common.adView = mAdView;

		mAdRequest = new AdRequest.Builder().build();
		common.adRequest = mAdRequest;
		
		//mAdView.loadAd(mAdRequest);

		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.main, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_easy:
			puzzleSurface.puzzleThread.setDifficulty(PuzzleSurface.EASY);
			return true;
		case R.id.action_hard:
			puzzleSurface.puzzleThread.setDifficulty(PuzzleSurface.HARD);
			return true;
		case R.id.action_very_hard:
			puzzleSurface.puzzleThread.setDifficulty(PuzzleSurface.VERY_HARD);
			return true;
		case R.id.action_save_image:
			new SavePhoto(common.currentPuzzleImagePosition).start();
			return true;
		case R.id.action_music_toggle:
			puzzleSurface.puzzleThread.toggleMusic();
			return true;
		case R.id.action_setsound_toggle:
			puzzleSurface.puzzleThread.toggleSetSound();
			return true;
		case R.id.action_chimesound_toggle:
			puzzleSurface.puzzleThread.toggleWinSound();
			return true;
		case R.id.action_border_toggle:
			puzzleSurface.puzzleThread.toggleBorder();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * makes the fragment visible to the user (based on its containing activity
	 * being started).
	 */
	@Override
	public void onStart() {
		super.onStart();
		if (puzzleSurface != null)
			puzzleSurface.onStart();
	}

	/**
	 * makes the fragments interacting with the user (based on its containing
	 * activity being resumed).
	 */
	@Override
	public void onResume() {
		super.onResume();
		if (puzzleSurface != null)
			puzzleSurface.resume();

		if (myMediaPlayer != null)
			myMediaPlayer.resume();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (myMediaPlayer != null)
			if (myMediaPlayer.mediaPlayer != null) {
				if (myMediaPlayer.mediaPlayer.isPlaying())
					myMediaPlayer.pause();
				common.currentSoundPosition = myMediaPlayer.mediaPlayer
						.getCurrentPosition();
			}
		myMediaPlayer.onPause();
		if (puzzleSurface != null)
			puzzleSurface.onPause();
	}

	/**
	 * fragment is no longer visible to the user either because its being
	 * stopped or a fragment operation is modifying it in the activity.
	 */
	@Override
	public void onStop() {
		super.onStop();
		Editor editor = sharedpreferences.edit();
		editor.putInt(COLUMN_DIFFICULTY, common.difficulty);
		editor.putInt(COLUMN_IMAGE_POSITION, common.currentPuzzleImagePosition);
		editor.putString(COLUMN_SLOTS, common.getSlotString());
		editor.putBoolean(COLUMN_SOUND, common.playTapSound);
		editor.putBoolean(COLUMN_MUSIC, common.playMusic);
		editor.putBoolean(COLUMN_CHIME, common.playChimeSound);
		editor.putBoolean(COLUMN_BORDER, common.drawBorders);
		editor.putInt(COLUMN_SOUND_POSITION, common.currentSoundPosition);
		editor.commit();
	}

	/**
	 * allows the fragment to clean up resources associated with its view.
	 */
	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	/**
	 * called immediately prior to the fragment no longer being associated with
	 * activity.
	 */
	@Override
	public void onDetach() {
		super.onDetach();
	}

	/**
	 * called to do final cleanup of the fragment's state.
	 */
	public void onDestroy() {
		super.onDestroy();
		if (mySoundPool != null) {
			mySoundPool.release();
			mySoundPool = null;
		}

		if (myMediaPlayer != null) {
			myMediaPlayer.cleanUp();
			myMediaPlayer = null;
		}

		if (puzzleSurface != null) {
			puzzleSurface.cleanUp();
			puzzleSurface = null;
		}

		common = null;

		// System.gc();
	}
}