package puzzle.ads;

import nasa.puzzles.R;
import android.media.SoundPool;

/**
 * 
 * Sound Pool extension includes loading sound and playing sounds.
 * 
 * @author Rick
 *
 */
public class MySoundPool extends SoundPool implements
		SoundPool.OnLoadCompleteListener {

	/**
	 * Constructor to sound pool object
	 * 
	 * @param maxStreams
	 *            are the max concurrent sounds to be played
	 * @param streamType
	 * @param srcQuality
	 */
	public MySoundPool(int maxStreams, int streamType, int srcQuality) {
		super(maxStreams, streamType, srcQuality);
		setOnLoadCompleteListener(this);
	}

	CommonVariables common = CommonVariables.getInstance();

	public void init() {
		common.tapSound = load(common.context, R.raw.tap01, 1);
		common.chimeSound = load(common.context, R.raw.chime, 1);
		// common.pageTurnSound = load(common.context, R.raw.pageturn, 1);
	}

	public void playTapSound() {
		if (common.tapLoaded && common.playTapSound) {
			play(common.tapSound, common.volume, common.volume, 1, 0, 1f);
		}
	}

	public void playChimeSound() {
		// check for sound file to be loaded and wanting to be player
		if (common.chimeLoaded && common.playChimeSound) {
			play(common.chimeSound, common.volume, common.volume, 1, 0, 1f);
		}
	}

	public void playPageTurnSound() {
		// check for tap sound to be loaded and it in preferences
		if (common.pageTurnLoaded && common.playPageTurnSound) {
			play(common.pageTurnSound, common.volume, common.volume, 1, 0, 1f);
		}
	}

	/**
	 * Match the status to the sound and set loaded for it.
	 */
	@Override
	public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
		if (sampleId == 1 && status == 0)
			common.tapLoaded = true;
		else if (sampleId == 2 && status == 0)
			common.chimeLoaded = true;
		// else if (sampleId == 3 && status == 0)
		// common.pageTurnLoaded = true;
	}
}